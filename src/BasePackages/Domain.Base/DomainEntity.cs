﻿using System;
using kevain.BasePackages.Contracts.Domain.Base;

namespace kevain.BasePackages.Domain.Base
{
    /// <inheritdoc cref="kevain.BasePackages.Contracts.Domain.Base.IDomainEntity" />
    public abstract class DomainEntity : DomainEntity<Guid>, IDomainEntity
    {
    }

    /// <inheritdoc />
    public abstract class DomainEntity<TKey> : IDomainEntity<TKey>
        where TKey : IEquatable<TKey>
    {
        /// <inheritdoc />
        public virtual TKey Id { get; set; } = default!;
    }
}