﻿using System;
using System.Threading.Tasks;
using kevain.BasePackages.Contracts.DAL.Base.Entities;
using kevain.BasePackages.Contracts.DAL.Base.Repositories;

namespace kevain.BasePackages.Contracts.DAL.Base.UnitOfWork
{
    /// <summary>
    ///     Defines a base for Unit of Work
    /// </summary>
    public interface IBaseUnitOfWork
    {
        /// <summary>
        ///     Save changes
        /// </summary>
        /// <returns>Number of affected items</returns>
        int SaveChanges();

        /// <summary>
        ///     Asynchronously save changes
        /// </summary>
        /// <returns>Number of affected items</returns>
        Task<int> SaveChangesAsync();

        /// <summary>
        ///     Get repository of given type. If repository exists in cache, then it will be returned from there,
        ///     otherwise a new instance will be created, cached and returned.
        /// </summary>
        /// <param name="repositoryInitializationMethod">
        ///     Method for initializing a new repository when repository with given type
        ///     could not be found
        /// </param>
        /// <typeparam name="TKey">Primary Key type</typeparam>
        /// <typeparam name="TDALEntity">Type of entity on DAL level</typeparam>
        /// <typeparam name="TRepository">Repository type</typeparam>
        /// <returns>Repository of provided type</returns>
        TRepository GetRepository<TKey, TDALEntity, TRepository>(Func<TRepository> repositoryInitializationMethod)
            where TKey : IEquatable<TKey>
            where TDALEntity : class, IDALEntity<TKey>, new()
            where TRepository : class, IBaseRepository<TKey, TDALEntity>;
    }
}