﻿using System;
using kevain.BasePackages.Contracts.DAL.Base.Entities;
using kevain.BasePackages.Contracts.Domain.Base;
using kevain.BasePackages.Contracts.Mapper.Base;

namespace kevain.BasePackages.Contracts.DAL.Base.Mappers
{
    /// <summary>
    ///     DAL-level mapper for mapping objects between Domain and DAL levels
    /// </summary>
    /// <typeparam name="TDomainEntity">Type of domain entity</typeparam>
    /// <typeparam name="TDALEntity">Type of DAL entity</typeparam>
    public interface IDALBaseMapper<TDomainEntity, TDALEntity> : IDALBaseMapper<Guid, TDomainEntity, TDALEntity>
        where TDomainEntity : class, IDomainEntity, new()
        where TDALEntity : class, IDALEntity, new()
    {
    }

    /// <summary>
    ///     DAL-level mapper for mapping objects between Domain and DAL levels
    /// </summary>
    /// <typeparam name="TKey">Primary Key type</typeparam>
    /// <typeparam name="TDomainEntity">Type of domain entity</typeparam>
    /// <typeparam name="TDALEntity">Type of DAL entity</typeparam>
    public interface IDALBaseMapper<in TKey, TDomainEntity, TDALEntity> : IBaseMapper<TDomainEntity, TDALEntity>
        where TDomainEntity : class, IDomainEntity<TKey>, new()
        where TDALEntity : class, IDALEntity<TKey>, new()
        where TKey : IEquatable<TKey>
    {
    }
}