﻿using System;

namespace kevain.BasePackages.Contracts.DAL.Base.Entities
{
    /// <summary>
    ///     DAL entity with GUID-based Id property
    /// </summary>
    public interface IDALEntity : IDALEntity<Guid>
    {
    }


    /// <summary>
    ///     DAL entity with Id property
    /// </summary>
    /// <typeparam name="TKey">Type of Id property</typeparam>
    public interface IDALEntity<TKey>
        where TKey : IEquatable<TKey>
    {
        /// <summary>
        ///     Entity's identifier
        /// </summary>
        TKey Id { get; set; }
    }
}