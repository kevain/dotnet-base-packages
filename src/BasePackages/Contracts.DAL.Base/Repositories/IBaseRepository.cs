﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using kevain.BasePackages.Contracts.DAL.Base.Entities;

namespace kevain.BasePackages.Contracts.DAL.Base.Repositories
{
    /// <summary>
    ///     Defines a repository with all basic operations (CRUD and existence)
    /// </summary>
    /// <typeparam name="TKey">Repository's primary key type</typeparam>
    /// <typeparam name="TDALEntity">DAL entity type</typeparam>
    public interface IBaseRepository<in TKey, TDALEntity>
        where TKey : IEquatable<TKey>
        where TDALEntity : class, IDALEntity<TKey>, new()
    {
        /// <summary>
        ///     Get all items.
        /// </summary>
        /// <param name="noTracking">Disable entity tracking on returned entities</param>
        /// <returns>Collection of entities</returns>
        IEnumerable<TDALEntity> GetAll(bool noTracking = true);

        /// <summary>
        ///     Get all items asynchronously.
        /// </summary>
        /// <param name="noTracking">Disable entity tracking on returned entities</param>
        /// <returns>Task, which resolves to collection of entities</returns>
        Task<IEnumerable<TDALEntity>> GetAllAsync(bool noTracking = true);

        /// <summary>
        ///     Get entity by ID.
        /// </summary>
        /// <param name="id">ID of entity being queried</param>
        /// <param name="noTracking">Disable entity tracking on returned entity</param>
        /// <returns>Entity with given ID value or default</returns>
        TDALEntity? FirstOrDefault(TKey id, bool noTracking = true);

        /// <summary>
        ///     Asynchronously get entity by ID.
        /// </summary>
        /// <param name="id">ID of entity being queried</param>
        /// <param name="noTracking">Disable entity tracking on returned entity</param>
        /// <returns>Task, which resolves to an entity with given ID value or default</returns>
        Task<TDALEntity?> FirstOrDefaultAsync(TKey id, bool noTracking = true);

        /// <summary>
        ///     Begin tracking new entity.
        /// </summary>
        /// <param name="entity">Entity to be tracked</param>
        /// <returns>Tracked entity</returns>
        TDALEntity Add(TDALEntity entity);

        /// <summary>
        ///     Begin tracking new entity.
        /// </summary>
        /// <param name="entity">Entity to be tracked</param>
        /// <returns>Tracked entity</returns>
        Task<TDALEntity> AddAsync(TDALEntity entity);

        /// <summary>
        ///     Begin tracking updated entity
        /// </summary>
        /// <param name="entity">Updated entity</param>
        /// <returns>Tracked updated entity</returns>
        TDALEntity Update(TDALEntity entity);

        /// <summary>
        ///     Begin tracking removed entity
        /// </summary>
        /// <param name="entity">Removed entity</param>
        /// <returns>Tracked removed entity</returns>
        TDALEntity Remove(TDALEntity entity);

        /// <summary>
        ///     Begin tracking removed entity
        /// </summary>
        /// <param name="id">Removed entity's ID</param>
        /// <returns>Tracked removed entity</returns>
        TDALEntity? Remove(TKey id);

        /// <summary>
        ///     Check for entity's existence in Db.
        /// </summary>
        /// <param name="id">Entity's ID</param>
        /// <returns>true/false whether entity exists</returns>
        bool Exists(TKey id);

        /// <summary>
        ///     Asynchronously check for entity's existence in Db.
        /// </summary>
        /// <param name="id">Entity's ID</param>
        /// <returns>Task, which resolves to true/false whether entity exists</returns>
        Task<bool> ExistsAsync(TKey id);
    }
}