﻿using System;

namespace kevain.BasePackages.Contracts.Domain.Base
{
    /// <summary>
    ///     Domain entity with GUID-based Id property
    /// </summary>
    public interface IDomainEntity : IDomainEntity<Guid>
    {
    }

    /// <summary>
    ///     Domain entity with ID property
    /// </summary>
    /// <typeparam name="TKey">Id property type</typeparam>
    public interface IDomainEntity<TKey>
        where TKey : IEquatable<TKey>
    {
        /// <summary>
        ///     Entity's identifier (Primary Key)
        /// </summary>
        TKey Id { get; set; }
    }
}