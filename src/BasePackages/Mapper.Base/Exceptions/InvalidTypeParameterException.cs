﻿using System;

namespace kevain.BasePackages.Mapper.Base.Exceptions
{
    /// <summary>
    ///     Invalid type parameter was provided to the BaseMapper's Map method call
    /// </summary>
    public class InvalidTypeParameterException : Exception
    {
        /// <summary>
        ///     Default constructor
        /// </summary>
        public InvalidTypeParameterException()
        {
        }

        /// <summary>
        ///     Constructor
        /// </summary>
        /// <param name="message">Exception message</param>
        public InvalidTypeParameterException(string message) : base(message)
        {
        }

        /// <summary>
        ///     Constructor
        /// </summary>
        /// <param name="message">Exception message</param>
        /// <param name="innerException">Inner exception</param>
        public InvalidTypeParameterException(string message, Exception innerException) : base(message, innerException)
        {
        }
    }
}