﻿using AutoMapper;
using AutoMapper.Configuration;
using kevain.BasePackages.Contracts.Mapper.Base;
using kevain.BasePackages.Mapper.Base.Exceptions;

namespace kevain.BasePackages.Mapper.Base
{
    /// <summary>
    ///     Automapper-based mapper for mapping objects back and forth between two provided types
    /// </summary>
    /// <typeparam name="TFirstObject">Type of first object</typeparam>
    /// <typeparam name="TSecondObject">Type of second object</typeparam>
    public class BaseMapper<TFirstObject, TSecondObject> : IBaseMapper<TFirstObject, TSecondObject>
        where TFirstObject : class?, new()
        where TSecondObject : class?, new()
    {
        /// <summary>
        ///     AutoMapper's Mapper
        /// </summary>
        // ReSharper disable once MemberCanBePrivate.Global
        // ReSharper disable once FieldCanBeMadeReadOnly.Global
        protected IMapper Mapper;

        /// <summary>
        ///     AutoMapper's configuration
        /// </summary>
        // ReSharper disable once MemberCanBePrivate.Global
        // ReSharper disable once FieldCanBeMadeReadOnly.Global
        protected MapperConfiguration MapperConfiguration;

        /// <summary>
        ///     AutoMapper's configuration expression
        /// </summary>
        // ReSharper disable once MemberCanBePrivate.Global
        // ReSharper disable once FieldCanBeMadeReadOnly.Global
        protected MapperConfigurationExpression MapperConfigurationExpression;

        /// <summary>
        ///     Constructor
        /// </summary>
        public BaseMapper()
        {
            MapperConfigurationExpression = new MapperConfigurationExpression();
            MapperConfigurationExpression.CreateMap<TFirstObject, TSecondObject>().ReverseMap();

            MapperConfiguration = new MapperConfiguration(MapperConfigurationExpression);

            Mapper = new AutoMapper.Mapper(MapperConfiguration);
        }

        /// <inheritdoc />
        public virtual TFirstObject Map(TSecondObject source) => Mapper.Map<TSecondObject, TFirstObject>(source);

        /// <inheritdoc />
        public virtual TSecondObject Map(TFirstObject source) => Mapper.Map<TFirstObject, TSecondObject>(source);
    }

    /// <summary>
    ///     AutoMapper-based mapper for mapping objects of three different types between each other
    /// </summary>
    /// <typeparam name="TFirstObject"></typeparam>
    /// <typeparam name="TSecondObject"></typeparam>
    /// <typeparam name="TThirdObject"></typeparam>
    // ReSharper disable once ClassWithVirtualMembersNeverInherited.Global
    public class BaseMapper<TFirstObject, TSecondObject, TThirdObject> : IBaseMapper<TFirstObject, TSecondObject,
        TThirdObject>
        where TFirstObject : class, new()
        where TSecondObject : class, new()
        where TThirdObject : class, new()
    {
        /// <summary>
        ///     AutoMapper's Mapper
        /// </summary>
        // ReSharper disable once MemberCanBePrivate.Global
        // ReSharper disable once FieldCanBeMadeReadOnly.Global
        protected IMapper Mapper;

        /// <summary>
        ///     AutoMapper's configuration
        /// </summary>
        // ReSharper disable once MemberCanBePrivate.Global
        // ReSharper disable once FieldCanBeMadeReadOnly.Global
        protected MapperConfiguration MapperConfiguration;

        /// <summary>
        ///     AutoMapper's configuration expression
        /// </summary>
        // ReSharper disable once MemberCanBePrivate.Global
        // ReSharper disable once FieldCanBeMadeReadOnly.Global
        protected MapperConfigurationExpression MapperConfigurationExpression;

        /// <summary>
        ///     Constructor
        /// </summary>
        public BaseMapper()
        {
            MapperConfigurationExpression = new MapperConfigurationExpression();
            MapperConfigurationExpression.CreateMap<TFirstObject, TSecondObject>().ReverseMap();
            MapperConfigurationExpression.CreateMap<TFirstObject, TThirdObject>().ReverseMap();
            MapperConfigurationExpression.CreateMap<TSecondObject, TThirdObject>().ReverseMap();

            MapperConfiguration = new MapperConfiguration(MapperConfigurationExpression);

            Mapper = new AutoMapper.Mapper(MapperConfiguration);
        }

        /// <inheritdoc />
        public virtual TDestination Map<TDestination>(TFirstObject source) where TDestination : class?, new()
        {
            if (typeof(TDestination) == typeof(TFirstObject)) return (source as TDestination)!;

            if (typeof(TDestination) == typeof(TSecondObject) || typeof(TDestination) == typeof(TThirdObject))
                return Mapper.Map<TFirstObject, TDestination>(source);

            throw new InvalidTypeParameterException(
                $"{typeof(TDestination)} is not supported as mapping destination type. " +
                "Supported destination types are " +
                $"[{typeof(TFirstObject)}, {typeof(TSecondObject)}, {typeof(TThirdObject)}]");
        }

        /// <inheritdoc />
        public virtual TDestination Map<TDestination>(TSecondObject source) where TDestination : class?, new()
        {
            if (typeof(TDestination) == typeof(TSecondObject)) return (source as TDestination)!;

            if (typeof(TDestination) == typeof(TFirstObject) || typeof(TDestination) == typeof(TThirdObject))
                return Mapper.Map<TSecondObject, TDestination>(source);

            throw new InvalidTypeParameterException(
                $"{typeof(TDestination)} is not supported as mapping destination type. " +
                "Supported destination types are " +
                $"[{typeof(TFirstObject)}, {typeof(TSecondObject)}, {typeof(TThirdObject)}]");
        }

        /// <inheritdoc />
        public virtual TDestination Map<TDestination>(TThirdObject source) where TDestination : class?, new()
        {
            if (typeof(TDestination) == typeof(TThirdObject)) return (source as TDestination)!;

            if (typeof(TDestination) == typeof(TFirstObject) || typeof(TDestination) == typeof(TSecondObject))
                return Mapper.Map<TThirdObject, TDestination>(source);

            throw new InvalidTypeParameterException(
                $"{typeof(TDestination)} is not supported as mapping destination type. " +
                "Supported destination types are " +
                $"[{typeof(TFirstObject)}, {typeof(TSecondObject)}, {typeof(TThirdObject)}]");
        }
    }
}