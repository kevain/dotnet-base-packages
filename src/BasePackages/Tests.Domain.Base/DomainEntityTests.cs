using System;
using kevain.BasePackages.Domain.Base;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;

namespace kevain.BasePackages.Tests.Domain.Base
{
    [TestClass]
    public class DomainEntityTests
    {
        [TestMethod]
        public void Init_NoTypeParameter_IdPropertyTypeShouldBeGuid()
        {
            var entity = new Mock<DomainEntity>().Object;

            Assert.IsInstanceOfType(entity.Id, typeof(Guid));
        }

        [TestMethod]
        public void Init_TypeParameterIsGuid_IdPropertyTypeShouldBeGuid()
        {
            var entity = new Mock<DomainEntity<Guid>>().Object;

            Assert.IsInstanceOfType(entity.Id, typeof(Guid));
        }

        [TestMethod]
        public void Init_NoTypeParameterIsInt_IdPropertyTypeShouldBeGuid()
        {
            var entity = new Mock<DomainEntity<int>>().Object;

            Assert.IsInstanceOfType(entity.Id, typeof(int));
        }
    }
}