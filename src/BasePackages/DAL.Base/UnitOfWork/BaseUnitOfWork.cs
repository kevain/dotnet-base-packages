﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using kevain.BasePackages.Contracts.DAL.Base.Entities;
using kevain.BasePackages.Contracts.DAL.Base.Repositories;
using kevain.BasePackages.Contracts.DAL.Base.UnitOfWork;

namespace kevain.BasePackages.DAL.Base.UnitOfWork
{
    /// <summary>
    ///     Basic Unit of Work class for saving changes and getting/storing repositories from/in cache.
    /// </summary>
    public abstract class BaseUnitOfWork : IBaseUnitOfWork
    {
        private readonly Dictionary<Type, object> _repositoryCache = new Dictionary<Type, object>();

        /// <inheritdoc />
        public abstract int SaveChanges();

        /// <inheritdoc />
        public abstract Task<int> SaveChangesAsync();

        /// <inheritdoc />
        public TRepository GetRepository<TKey, TDALEntity, TRepository>(
            Func<TRepository> repositoryInitializationMethod)
            where TKey : IEquatable<TKey>
            where TDALEntity : class, IDALEntity<TKey>, new()
            where TRepository : class, IBaseRepository<TKey, TDALEntity>
        {
            if (_repositoryCache.TryGetValue(typeof(TRepository), out var repository)) return (TRepository) repository;

            repository = repositoryInitializationMethod();
            _repositoryCache.Add(typeof(TRepository), repository);

            return (TRepository) repository;
        }
    }
}