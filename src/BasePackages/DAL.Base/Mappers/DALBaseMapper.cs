﻿using System;
using kevain.BasePackages.Contracts.DAL.Base.Entities;
using kevain.BasePackages.Contracts.DAL.Base.Mappers;
using kevain.BasePackages.Contracts.Domain.Base;
using kevain.BasePackages.Mapper.Base;

namespace kevain.BasePackages.DAL.Base.Mappers
{
    /// <summary>
    ///     DAL-level mapper for mapping entities between Domain and DAL levels
    /// </summary>
    /// <typeparam name="TDomainEntity">Type of domain entity</typeparam>
    /// <typeparam name="TDALEntity">Type of DAL entity</typeparam>
    public class DALBaseMapper<TDomainEntity, TDALEntity> : DALBaseMapper<Guid, TDomainEntity, TDALEntity>,
        IDALBaseMapper<TDomainEntity, TDALEntity>
        where TDomainEntity : class, IDomainEntity, new()
        where TDALEntity : class, IDALEntity, new()
    {
    }

    /// <summary>
    ///     DAL-level mapper for mapping entities between Domain and DAL levels
    /// </summary>
    /// <typeparam name="TKey">Primary Key type</typeparam>
    /// <typeparam name="TDomainEntity">Type of domain entity</typeparam>
    /// <typeparam name="TDALEntity">Type of DAL entity</typeparam>
    public class DALBaseMapper<TKey, TDomainEntity, TDALEntity> : BaseMapper<TDomainEntity, TDALEntity>,
        IDALBaseMapper<TKey, TDomainEntity, TDALEntity>
        where TDomainEntity : class, IDomainEntity<TKey>, new()
        where TDALEntity : class, IDALEntity<TKey>, new()
        where TKey : IEquatable<TKey>
    {
    }
}