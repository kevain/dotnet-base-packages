﻿using System;
using kevain.BasePackages.Contracts.DAL.Base.Entities;

namespace kevain.BasePackages.DAL.Base.Entities
{
    /// <inheritdoc cref="kevain.BasePackages.Contracts.DAL.Base.Entities.IDALEntity" />
    public abstract class DALEntity : DALEntity<Guid>, IDALEntity
    {
    }

    /// <inheritdoc />
    public abstract class DALEntity<TKey> : IDALEntity<TKey>
        where TKey : IEquatable<TKey>
    {
        /// <inheritdoc />
        public virtual TKey Id { get; set; } = default!;
    }
}