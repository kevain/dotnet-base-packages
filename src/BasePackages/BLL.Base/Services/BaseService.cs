﻿using kevain.BasePackages.Contracts.BLL.Base.Services;

namespace kevain.BasePackages.BLL.Base.Services
{
    /// <summary>
    ///     Provides a base for service
    /// </summary>
    public abstract class BaseService : IBaseService
    {
    }
}