﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using kevain.BasePackages.Contracts.BLL.Base.Entities;
using kevain.BasePackages.Contracts.BLL.Base.Mappers;
using kevain.BasePackages.Contracts.BLL.Base.Services;
using kevain.BasePackages.Contracts.DAL.Base.Entities;
using kevain.BasePackages.Contracts.DAL.Base.Repositories;
using kevain.BasePackages.Contracts.DAL.Base.UnitOfWork;

namespace kevain.BasePackages.BLL.Base.Services
{
    /// <summary>
    ///     Base service, which provides CRUD and existence operations for provided entity type
    /// </summary>
    /// <typeparam name="TKey">Primary key type</typeparam>
    /// <typeparam name="TUnitOfWork">Unit of Work type</typeparam>
    /// <typeparam name="TRepository">Repository type</typeparam>
    /// <typeparam name="TMapper">Mapper type</typeparam>
    /// <typeparam name="TDALEntity">DAL entity type</typeparam>
    /// <typeparam name="TBLLEntity">BLL entity type</typeparam>
    public abstract class BaseEntityService<TKey, TUnitOfWork, TRepository, TMapper, TDALEntity, TBLLEntity> :
        IBaseEntityService<TKey, TBLLEntity>
        where TKey : IEquatable<TKey>
        where TUnitOfWork : IBaseUnitOfWork
        where TRepository : IBaseRepository<TKey, TDALEntity>
        where TMapper : IBLLBaseMapper<TKey, TDALEntity, TBLLEntity>
        where TDALEntity : class, IDALEntity<TKey>, new()
        where TBLLEntity : class, IBLLEntity<TKey>, new()
    {
        /// <summary>
        ///     Object mapper
        /// </summary>
        // ReSharper disable once MemberCanBePrivate.Global
        protected readonly TMapper Mapper;

        /// <summary>
        ///     Repository
        /// </summary>
        // ReSharper disable once MemberCanBePrivate.Global
        protected readonly TRepository Repository;

        /// <summary>
        ///     Unit of Work
        /// </summary>
        // ReSharper disable once NotAccessedField.Global
        // ReSharper disable once MemberCanBePrivate.Global
        protected readonly TUnitOfWork UnitOfWork;

        /// <summary>
        ///     Constructor
        /// </summary>
        /// <param name="unitOfWork">Unit of Work</param>
        /// <param name="repository">Repository</param>
        /// <param name="mapper">Object mapper</param>
        protected BaseEntityService(TUnitOfWork unitOfWork, TRepository repository, TMapper mapper)
        {
            UnitOfWork = unitOfWork;
            Repository = repository;
            Mapper = mapper;
        }

        /// <inheritdoc />
        public virtual IEnumerable<TBLLEntity> GetAll(bool noTracking = true)
        {
            var dalEntities = Repository.GetAll(noTracking);
            var bllEntities = dalEntities.Select(dalEntity => Mapper.Map(dalEntity));

            return bllEntities;
        }

        /// <inheritdoc />
        public virtual async Task<IEnumerable<TBLLEntity>> GetAllAsync(bool noTracking = true)
        {
            var dalEntities = await Repository.GetAllAsync(noTracking);
            var bllEntities = dalEntities.Select(dalEntity => Mapper.Map(dalEntity));

            return bllEntities;
        }

        /// <inheritdoc />
        public virtual TBLLEntity? FirstOrDefault(TKey id, bool noTracking = true)
        {
            var dalEntity = Repository.FirstOrDefault(id, noTracking);

            return dalEntity != null ? Mapper.Map(dalEntity) : null;
        }

        /// <inheritdoc />
        public virtual async Task<TBLLEntity?> FirstOrDefaultAsync(TKey id, bool noTracking = true)
        {
            var dalEntity = await Repository.FirstOrDefaultAsync(id, noTracking);

            return dalEntity != null ? Mapper.Map(dalEntity) : null;
        }

        /// <inheritdoc />
        public virtual TBLLEntity Add(TBLLEntity entity)
        {
            var dalEntity = Mapper.Map(entity);
            var trackedDALEntity = Repository.Add(dalEntity);
            var bllEntity = Mapper.Map(trackedDALEntity);

            return bllEntity;
        }

        /// <inheritdoc />
        public virtual async Task<TBLLEntity> AddAsync(TBLLEntity entity)
        {
            var dalEntity = Mapper.Map(entity);
            var trackedDALEntity = await Repository.AddAsync(dalEntity);
            var bllEntity = Mapper.Map(trackedDALEntity);

            return bllEntity;
        }

        /// <inheritdoc />
        public virtual TBLLEntity Update(TBLLEntity entity)
        {
            var dalEntity = Mapper.Map(entity);
            var updatedDALEntity = Repository.Update(dalEntity);
            var bllEntity = Mapper.Map(updatedDALEntity);

            return bllEntity;
        }

        /// <inheritdoc />
        public virtual TBLLEntity Remove(TBLLEntity entity)
        {
            var dalEntity = Mapper.Map(entity);
            var removedDALEntity = Repository.Remove(dalEntity);
            var bllEntity = Mapper.Map(removedDALEntity);

            return bllEntity;
        }

        /// <inheritdoc />
        public virtual TBLLEntity? Remove(TKey id)
        {
            var removedDALEntity = Repository.Remove(id);

            return removedDALEntity != null ? Mapper.Map(removedDALEntity) : null;
        }

        /// <inheritdoc />
        public virtual bool Exists(TKey id) => Repository.Exists(id);

        /// <inheritdoc />
        public virtual async Task<bool> ExistsAsync(TKey id) => await Repository.ExistsAsync(id);
    }
}