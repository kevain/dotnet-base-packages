﻿using System;
using kevain.BasePackages.Contracts.BLL.Base.Entities;

namespace kevain.BasePackages.BLL.Base.Entities
{
    /// <inheritdoc cref="kevain.BasePackages.Contracts.BLL.Base.Entities.IBLLEntity" />
    public abstract class BLLEntity : BLLEntity<Guid>, IBLLEntity
    {
    }

    /// <inheritdoc />
    public abstract class BLLEntity<TKey> : IBLLEntity<TKey>
        where TKey : IEquatable<TKey>
    {
        /// <inheritdoc />
        public TKey Id { get; set; } = default!;
    }
}