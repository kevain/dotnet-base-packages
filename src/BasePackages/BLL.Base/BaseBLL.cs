﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using kevain.BasePackages.Contracts.BLL.Base;
using kevain.BasePackages.Contracts.BLL.Base.Services;
using kevain.BasePackages.Contracts.DAL.Base.UnitOfWork;

namespace kevain.BasePackages.BLL.Base
{
    /// <summary>
    ///     Provides a basic BLL class with access to all the services.
    /// </summary>
    /// <typeparam name="TUnitOfWork">Unit of Work type</typeparam>
    public abstract class BaseBLL<TUnitOfWork> : IBaseBLL
        where TUnitOfWork : IBaseUnitOfWork
    {
        private readonly Dictionary<Type, object> _serviceCache = new Dictionary<Type, object>();

        /// <summary>
        ///     Unit of Work
        /// </summary>
        // ReSharper disable once MemberCanBePrivate.Global
        protected readonly TUnitOfWork UnitOfWork;

        /// <summary>
        ///     Constructor
        /// </summary>
        /// <param name="unitOfWork">Unit of Work</param>
        protected BaseBLL(TUnitOfWork unitOfWork)
        {
            UnitOfWork = unitOfWork;
        }

        /// <inheritdoc />
        public int SaveChanges() => UnitOfWork.SaveChanges();

        /// <inheritdoc />
        public async Task<int> SaveChangesAsync() => await UnitOfWork.SaveChangesAsync();

        /// <inheritdoc />
        public TService GetService<TService>(Func<TService> serviceInitializationMethod)
            where TService : class, IBaseService
        {
            if (_serviceCache.TryGetValue(typeof(TService), out var service)) return (TService) service;

            var newServiceInstance = serviceInitializationMethod();
            _serviceCache.Add(typeof(TService), newServiceInstance);
            return newServiceInstance;
        }
    }
}