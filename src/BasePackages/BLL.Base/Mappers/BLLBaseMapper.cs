﻿using kevain.BasePackages.Contracts.BLL.Base.Entities;
using kevain.BasePackages.Contracts.BLL.Base.Mappers;
using kevain.BasePackages.Contracts.DAL.Base.Entities;
using kevain.BasePackages.Mapper.Base;

namespace kevain.BasePackages.BLL.Base.Mappers
{
    /// <summary>
    ///     BLL-level mapper for mapping entity back and forth between DAL and BLL DTOs
    /// </summary>
    /// <typeparam name="TDALEntity">Type of DAL entity</typeparam>
    /// <typeparam name="TBLLEntity">Type of BLL entity</typeparam>
    public class BLLBaseMapper<TDALEntity, TBLLEntity> : BaseMapper<TDALEntity, TBLLEntity>,
        IBLLBaseMapper<TDALEntity, TBLLEntity>
        where TDALEntity : class, IDALEntity, new()
        where TBLLEntity : class, IBLLEntity, new()
    {
    }
}