﻿using System;
using kevain.BasePackages.DAL.Base.Entities;

namespace kevain.BasePackages.Tests.DAL.Base.Helpers
{
    public class TestDALEntity : DALEntity
    {
    }

    public class TestDALEntity<TKey> : DALEntity<TKey>
        where TKey : IEquatable<TKey>
    {
    }
}