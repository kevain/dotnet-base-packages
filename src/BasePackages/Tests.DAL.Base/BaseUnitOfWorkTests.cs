using System;
using Autofac.Extras.Moq;
using kevain.BasePackages.Contracts.DAL.Base.Repositories;
using kevain.BasePackages.Contracts.DAL.Base.UnitOfWork;
using kevain.BasePackages.DAL.Base.UnitOfWork;
using kevain.BasePackages.Tests.DAL.Base.Helpers;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;

namespace kevain.BasePackages.Tests.DAL.Base
{
    [TestClass]
    public class BaseUnitOfWorkTests
    {
        private IBaseUnitOfWork _unitOfWork = null!;

        [TestMethod]
        public void GetRepository_InitializingNewRepository_ReturnsInitializedRepository()
        {
            using var mock = AutoMock.GetLoose();
            _unitOfWork = mock.Mock<BaseUnitOfWork>().Object;

            var cachedRepository =
                _unitOfWork.GetRepository<Guid, TestDALEntity, IBaseRepository<Guid, TestDALEntity>>(
                    () => new Mock<IBaseRepository<Guid, TestDALEntity>>().Object);

            Assert.IsInstanceOfType(cachedRepository, typeof(IBaseRepository<Guid, TestDALEntity>));
        }

        [TestMethod]
        public void GetRepository_QueryingSameRepositoryMultipleTimes_ReturnsSameInstancesOfRepository()
        {
            using var mock = AutoMock.GetLoose();
            _unitOfWork = mock.Mock<BaseUnitOfWork>().Object;

            var cachedRepository1 =
                _unitOfWork.GetRepository<Guid, TestDALEntity, IBaseRepository<Guid, TestDALEntity>>(
                    () => new Mock<IBaseRepository<Guid, TestDALEntity>>().Object);

            var cachedRepository2 =
                _unitOfWork.GetRepository<Guid, TestDALEntity, IBaseRepository<Guid, TestDALEntity>>(
                    () => new Mock<IBaseRepository<Guid, TestDALEntity>>().Object);

            Assert.AreSame(cachedRepository1, cachedRepository2);
        }
    }
}