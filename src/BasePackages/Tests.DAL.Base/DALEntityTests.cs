﻿using System;
using kevain.BasePackages.Tests.DAL.Base.Helpers;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace kevain.BasePackages.Tests.DAL.Base
{
    [TestClass]
    public class DALEntityTests
    {
        [TestMethod]
        public void Init_NoSpecifiedKeyType_PrimaryKeyIsOfTypeGuid()
        {
            var dalEntity = new TestDALEntity();
                
            Assert.IsInstanceOfType(dalEntity.Id, typeof(Guid));
        }
        
        [TestMethod]
        public void Init_KeyTypeGuid_PrimaryKeyIsOfTypeGuid()
        {
            var dalEntity = new TestDALEntity<Guid>();
                
            Assert.IsInstanceOfType(dalEntity.Id, typeof(Guid));
        }
        
        [TestMethod]
        public void Init_KeyTypeInt_PrimaryKeyIsOfTypeGuid()
        {
            var dalEntity = new TestDALEntity<int>();
                
            Assert.IsInstanceOfType(dalEntity.Id, typeof(int));
        }
    }
}