#pragma warning disable 1591

using System;
using Faker;
using kevain.BasePackages.Contracts.Mapper.Base;
using kevain.BasePackages.Mapper.Base;
using kevain.BasePackages.Tests.Mapper.Base.Helpers;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace kevain.BasePackages.Tests.Mapper.Base
{
    [TestClass]
    public class BaseTwoWayMapperTests
    {
        private readonly IBaseMapper<DummyOne, DummyTwo> _mapper = new BaseMapper<DummyOne, DummyTwo>();

        [TestMethod]
        public void Map_FromFirstTypeToSecond_MappedObjectIsOfSecondType()
        {
            var objectOfFirstType = new DummyOne();

            var mappedObject = _mapper.Map(objectOfFirstType);

            Assert.AreEqual(mappedObject.GetType(), typeof(DummyTwo));
        }

        [TestMethod]
        public void Map_FromSecondTypeToFirst_MappedObjectIsOfFirstType()
        {
            var objectOfSecondType = new DummyTwo();

            var mappedObject = _mapper.Map(objectOfSecondType);

            Assert.AreEqual(mappedObject.GetType(), typeof(DummyOne));
        }

        [TestMethod]
        public void Map_FromFirstTypeToSecond_ShouldRetainSameIDPropertyValue()
        {
            var objectOfFirstType = new DummyOne
            {
                Id = Guid.NewGuid()
            };

            var mappedObject = _mapper.Map(objectOfFirstType);

            Assert.AreEqual(mappedObject.Id, objectOfFirstType.Id);
        }

        [TestMethod]
        public void Map_FromFirstTypeToSecond_ShouldRetainSameNumberPropertyValue()
        {
            var objectOfFirstType = new DummyOne
            {
                Number = RandomNumber.Next()
            };

            var mappedObject = _mapper.Map(objectOfFirstType);

            Assert.AreEqual(mappedObject.Number, objectOfFirstType.Number);
        }

        [TestMethod]
        public void Map_FromFirstTypeToSecond_ShouldRetainSameTextPropertyValue()
        {
            var objectOfFirstType = new DummyOne
            {
                Text = Lorem.Sentence()
            };

            var mappedObject = _mapper.Map(objectOfFirstType);

            Assert.AreEqual(mappedObject.Text, objectOfFirstType.Text);
        }

        [TestMethod]
        public void Map_FromSecondTypeToFirst_ShouldRetainSameIDPropertyValue()
        {
            var objectOfFirstType = new DummyTwo
            {
                Id = Guid.NewGuid()
            };

            var mappedObject = _mapper.Map(objectOfFirstType);

            Assert.AreEqual(mappedObject.Id, objectOfFirstType.Id);
        }

        [TestMethod]
        public void Map_FromSecondTypeToFirst_ShouldRetainSameNumberPropertyValue()
        {
            var objectOfFirstType = new DummyTwo
            {
                Number = RandomNumber.Next()
            };

            var mappedObject = _mapper.Map(objectOfFirstType);

            Assert.AreEqual(mappedObject.Number, objectOfFirstType.Number);
        }

        [TestMethod]
        public void Map_FromSecondTypeToFirst_ShouldRetainSameTextPropertyValue()
        {
            var objectOfFirstType = new DummyTwo
            {
                Text = Lorem.Sentence()
            };

            var mappedObject = _mapper.Map(objectOfFirstType);

            Assert.AreEqual(mappedObject.Text, objectOfFirstType.Text);
        }
    }
}