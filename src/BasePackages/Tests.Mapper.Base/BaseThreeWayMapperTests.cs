﻿using System;
using kevain.BasePackages.Contracts.Mapper.Base;
using kevain.BasePackages.Mapper.Base;
using kevain.BasePackages.Mapper.Base.Exceptions;
using kevain.BasePackages.Tests.Mapper.Base.Helpers;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace kevain.BasePackages.Tests.Mapper.Base
{
    [TestClass]
    public class BaseThreeWayMapperTests
    {
        private readonly IBaseMapper<DummyOne, DummyTwo, DummyThree> _mapper =
            new BaseMapper<DummyOne, DummyTwo, DummyThree>();

        #region TypeParam1 tests

        [TestMethod]
        public void Map_TypeOneToTypeThree_ReturnsTypeThreeObject()
        {
            var source = new DummyOne();
            var destination = _mapper.Map<DummyThree>(source);

            Assert.IsInstanceOfType(destination, typeof(DummyThree));
        }

        [TestMethod]
        public void Map_TypeOneToTypeTwo_ReturnsTypeTwoObject()
        {
            var source = new DummyOne();
            var destination = _mapper.Map<DummyTwo>(source);

            Assert.IsInstanceOfType(destination, typeof(DummyTwo));
        }

        [TestMethod]
        public void Map_TypeOneToTypeOne_ReturnsTypeOneObject()
        {
            var source = new DummyOne();
            var destination = _mapper.Map<DummyOne>(source);

            Assert.IsInstanceOfType(destination, typeof(DummyOne));
        }

        [TestMethod]
        public void Map_TypeOneToTypeOne_ReturnsSameObject()
        {
            var source = new DummyOne();
            var destination = _mapper.Map<DummyOne>(source);

            Assert.AreSame(source, destination);
        }

        [TestMethod]
        public void Map_TypeOneToInvalidType_ThrowsException()
        {
            var source = new DummyOne();
            var mappingCallback = new Func<object>(() => _mapper.Map<object>(source));

            Assert.ThrowsException<InvalidTypeParameterException>(mappingCallback);
        }

        #endregion

        #region TypeParam2 tests

        [TestMethod]
        public void Map_TypeTwoToTypeOne_ReturnsTypeOneObject()
        {
            var source = new DummyTwo();
            var destination = _mapper.Map<DummyOne>(source);

            Assert.IsInstanceOfType(destination, typeof(DummyOne));
        }

        [TestMethod]
        public void Map_TypeTwoToTypeTwo_ReturnsTypeTwoObject()
        {
            var source = new DummyTwo();
            var destination = _mapper.Map<DummyTwo>(source);

            Assert.IsInstanceOfType(destination, typeof(DummyTwo));
        }

        [TestMethod]
        public void Map_TypeTwoToTypeTwo_ReturnsSameObject()
        {
            var source = new DummyTwo();
            var destination = _mapper.Map<DummyTwo>(source);

            Assert.AreSame(source, destination);
        }

        [TestMethod]
        public void Map_TypeTwoToTypeThree_ReturnsTypeThreeObject()
        {
            var source = new DummyTwo();
            var destination = _mapper.Map<DummyThree>(source);

            Assert.IsInstanceOfType(destination, typeof(DummyThree));
        }

        [TestMethod]
        public void Map_TypeTwoToInvalidType_ThrowsException()
        {
            var source = new DummyTwo();
            var mappingCallback = new Func<object>(() => _mapper.Map<object>(source));

            Assert.ThrowsException<InvalidTypeParameterException>(mappingCallback);
        }

        #endregion

        #region TypeParam3 tests

        [TestMethod]
        public void Map_TypeThreeToTypeOne_ReturnsTypeOneObject()
        {
            var source = new DummyThree();
            var destination = _mapper.Map<DummyOne>(source);

            Assert.IsInstanceOfType(destination, typeof(DummyOne));
        }

        [TestMethod]
        public void Map_TypeThreeToTypeTwo_ReturnsTypeTwoObject()
        {
            var source = new DummyThree();
            var destination = _mapper.Map<DummyTwo>(source);

            Assert.IsInstanceOfType(destination, typeof(DummyTwo));
        }

        [TestMethod]
        public void Map_TypeThreeToTypeThree_ReturnsSameObject()
        {
            var source = new DummyThree();
            var destination = _mapper.Map<DummyThree>(source);

            Assert.AreSame(source, destination);
        }

        [TestMethod]
        public void Map_TypeThreeToTypeThree_ReturnsTypeThreeObject()
        {
            var source = new DummyThree();
            var destination = _mapper.Map<DummyThree>(source);

            Assert.IsInstanceOfType(destination, typeof(DummyThree));
        }

        [TestMethod]
        public void Map_TypeThreeToInvalidType_ThrowsException()
        {
            var source = new DummyThree();
            var mappingCallback = new Func<object>(() => _mapper.Map<object>(source));

            Assert.ThrowsException<InvalidTypeParameterException>(mappingCallback);
        }

        #endregion
    }
}