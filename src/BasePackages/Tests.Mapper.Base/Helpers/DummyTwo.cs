﻿#pragma warning disable 1591
using System;

namespace kevain.BasePackages.Tests.Mapper.Base.Helpers
{
    public class DummyTwo : IDummy
    {
        public Guid Id { get; set; }
        public int Number { get; set; }
        public string Text { get; set; } = default!;
    }
}