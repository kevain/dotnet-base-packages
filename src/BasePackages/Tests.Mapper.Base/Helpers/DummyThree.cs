﻿using System;

namespace kevain.BasePackages.Tests.Mapper.Base.Helpers
{
    public class DummyThree : IDummy
    {
        public Guid Id { get; set; } = default!;
        public int Number { get; set; } = default!;
        public string Text { get; set; } = default!;
    }
}