﻿using System;

#pragma warning disable 1591
namespace kevain.BasePackages.Tests.Mapper.Base.Helpers
{
    public interface IDummy
    {
        Guid Id { get; set; }
        int Number { get; set; }
        string Text { get; set; }
    }
}