﻿using System;

#pragma warning disable 1591
namespace kevain.BasePackages.Tests.Mapper.Base.Helpers
{
    public class DummyOne: IDummy
    {
        public Guid Id { get; set; }
        public int Number { get; set; }
        public string Text { get; set; } = default!;
    }
}