﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using kevain.BasePackages.Contracts.DAL.Base.Entities;
using kevain.BasePackages.Contracts.DAL.Base.Repositories;
using kevain.BasePackages.Contracts.Domain.Base;
using kevain.BasePackages.Contracts.Mapper.Base;
using Microsoft.EntityFrameworkCore;

namespace kevain.BasePackages.DAL.Base.EF.Repository
{
    /// <summary>
    ///     Provides default DB operations (CRUD and existence) using Microsoft's EF Core ORM
    /// </summary>
    /// <typeparam name="TKey">Primary key type</typeparam>
    /// <typeparam name="TDbContext">Database context type</typeparam>
    /// <typeparam name="TDomainEntity">Domain entity type</typeparam>
    /// <typeparam name="TDALEntity">DAL entity type</typeparam>
    public abstract class EFBaseRepository<TKey, TDbContext, TDomainEntity, TDALEntity> :
        IBaseRepository<TKey, TDALEntity>
        where TKey : IEquatable<TKey>
        where TDbContext : DbContext
        where TDomainEntity : class, IDomainEntity<TKey>, new()
        where TDALEntity : class, IDALEntity<TKey>, new()
    {
        /// <summary>
        ///     Mapper for mapping objects between domain and DAL entities.
        /// </summary>
        // ReSharper disable once MemberCanBePrivate.Global
        protected readonly IBaseMapper<TDomainEntity, TDALEntity> Mapper;

        /// <summary>
        ///     Database context.
        /// </summary>
        // ReSharper disable once MemberCanBePrivate.Global
        protected readonly TDbContext RepositoryDbContext;

        /// <summary>
        ///     Domain entity's DB Set in database context.
        /// </summary>
        // ReSharper disable once MemberCanBePrivate.Global
        protected readonly DbSet<TDomainEntity> RepositoryDbSet;

        /// <summary>
        ///     Constructor
        /// </summary>
        /// <param name="repositoryDbContext">Database context</param>
        /// <param name="mapper">Mapper</param>
        /// <exception cref="ArgumentNullException">DbSet could not be located</exception>
        protected EFBaseRepository(TDbContext repositoryDbContext, IBaseMapper<TDomainEntity, TDALEntity> mapper)
        {
            RepositoryDbContext = repositoryDbContext;
            RepositoryDbSet = RepositoryDbContext.Set<TDomainEntity>();

            if (RepositoryDbSet == null)
                throw new ArgumentNullException($"{typeof(TDomainEntity).Name} was not found as a DbSet!");

            Mapper = mapper;
        }

        /// <inheritdoc />
        public virtual IEnumerable<TDALEntity> GetAll(bool noTracking = true)
        {
            var query = PrepareQuery(noTracking);
            var domainEntities = query.ToList();
            var dalEntities = domainEntities.Select(entity => Mapper.Map(entity));

            return dalEntities;
        }

        /// <inheritdoc />
        public virtual async Task<IEnumerable<TDALEntity>> GetAllAsync(bool noTracking = true)
        {
            var query = PrepareQuery(noTracking);
            var domainEntities = await query.ToListAsync();
            var dalEntities = domainEntities.Select(entity => Mapper.Map(entity));

            return dalEntities;
        }

        /// <inheritdoc />
        public virtual TDALEntity? FirstOrDefault(TKey id, bool noTracking = true)
        {
            var query = PrepareQuery(noTracking);
            var domainEntity = query.FirstOrDefault(entity => entity.Id.Equals(id));

            return domainEntity != null ? Mapper.Map(domainEntity) : null;
        }

        /// <inheritdoc />
        public virtual async Task<TDALEntity?> FirstOrDefaultAsync(TKey id, bool noTracking = true)
        {
            var query = PrepareQuery(noTracking);
            var domainEntity = await query.FirstOrDefaultAsync(entity => entity.Id.Equals(id));

            return domainEntity != null ? Mapper.Map(domainEntity) : null;
        }

        /// <inheritdoc />
        public virtual TDALEntity Add(TDALEntity entity)
        {
            var domainEntity = Mapper.Map(entity);
            var entityEntry = RepositoryDbSet.Add(domainEntity).Entity;
            var dalEntity = Mapper.Map(entityEntry);

            return dalEntity;
        }

        /// <inheritdoc />
        public virtual async Task<TDALEntity> AddAsync(TDALEntity entity)
        {
            var domainEntity = Mapper.Map(entity);
            var entityEntry = (await RepositoryDbSet.AddAsync(domainEntity)).Entity;
            var dalEntity = Mapper.Map(entityEntry);

            return dalEntity;
        }

        /// <inheritdoc />
        public virtual TDALEntity Update(TDALEntity entity)
        {
            var domainEntity = Mapper.Map(entity);
            var trackedDomainEntity = RepositoryDbSet.Update(domainEntity).Entity;
            var dalEntity = Mapper.Map(trackedDomainEntity);

            return dalEntity;
        }

        /// <inheritdoc />
        public virtual TDALEntity Remove(TDALEntity entity)
        {
            var domainEntity = Mapper.Map(entity);
            var dalEntity = Mapper.Map(RepositoryDbSet.Remove(domainEntity).Entity);

            return dalEntity;
        }

        /// <inheritdoc />
        public virtual TDALEntity? Remove(TKey id)
        {
            var dalEntity = FirstOrDefault(id);

            if (dalEntity == null) return null;

            var domainEntity = Mapper.Map(dalEntity);
            dalEntity = Mapper.Map(RepositoryDbSet.Remove(domainEntity).Entity);

            return dalEntity;
        }

        /// <inheritdoc />
        public virtual bool Exists(TKey id)
        {
            var query = PrepareQuery();
            var exists = query.Any(entity => entity.Id.Equals(id));

            return exists;
        }

        /// <inheritdoc />
        public virtual async Task<bool> ExistsAsync(TKey id)
        {
            var query = PrepareQuery();
            var exists = await query.AnyAsync(entity => entity.Id.Equals(id));

            return exists;
        }

        /// <summary>
        ///     Helper method for preparing the query.
        /// </summary>
        /// <param name="noTracking">Disable entity tracking</param>
        /// <returns>Domain entity query</returns>
        protected virtual IQueryable<TDomainEntity> PrepareQuery(bool noTracking = true)
        {
            var query = RepositoryDbSet.AsQueryable();

            if (noTracking) query = query.AsNoTracking();

            return query;
        }
    }
}