﻿using System.Threading.Tasks;
using kevain.BasePackages.DAL.Base.UnitOfWork;
using Microsoft.EntityFrameworkCore;

namespace kevain.BasePackages.DAL.Base.EF.UnitOfWork
{
    /// <summary>
    ///     Provides UoW functionality based on EF Core
    /// </summary>
    /// <typeparam name="TDbContext">Database context's type</typeparam>
    public abstract class EFBaseUnitOfWork<TDbContext> : BaseUnitOfWork
        where TDbContext : DbContext
    {
        /// <summary>
        ///     Database context
        /// </summary>
        // ReSharper disable once MemberCanBePrivate.Global
        protected readonly TDbContext UoWDbContext;

        /// <summary>
        ///     Constructor
        /// </summary>
        /// <param name="uoWDbContext">Database context</param>
        protected EFBaseUnitOfWork(TDbContext uoWDbContext)
        {
            UoWDbContext = uoWDbContext;
        }


        /// <inheritdoc />
        public override int SaveChanges() => UoWDbContext.SaveChanges();

        /// <inheritdoc />
        public override async Task<int> SaveChangesAsync() => await UoWDbContext.SaveChangesAsync();
    }
}