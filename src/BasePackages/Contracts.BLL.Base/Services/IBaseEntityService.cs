﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using kevain.BasePackages.Contracts.BLL.Base.Entities;

namespace kevain.BasePackages.Contracts.BLL.Base.Services
{
    /// <summary>
    ///     Base service, which provides CRUD and existence operations for provided entity type
    /// </summary>
    /// <typeparam name="TKey">Type of entity's ID property</typeparam>
    /// <typeparam name="TBLLEntity">Type of BLL entity</typeparam>
    public interface IBaseEntityService<in TKey, TBLLEntity> : IBaseService
        where TKey : IEquatable<TKey>
        where TBLLEntity : class, IBLLEntity<TKey>, new()
    {
        /// <summary>
        ///     Get all entities
        /// </summary>
        /// <param name="noTracking">Disable tracking of returned entities</param>
        /// <returns>Collection of entities</returns>
        IEnumerable<TBLLEntity> GetAll(bool noTracking = true);

        /// <summary>
        ///     Asynchronously get all entities
        /// </summary>
        /// <param name="noTracking">Disable tracking of returned entities</param>
        /// <returns>Task, which resolves to collection of entities</returns>
        Task<IEnumerable<TBLLEntity>> GetAllAsync(bool noTracking = true);

        /// <summary>
        ///     Get entity by it's ID value
        /// </summary>
        /// <param name="id">Entity's ID value</param>
        /// <param name="noTracking">Disable tracking of returned entity</param>
        /// <returns>Entity</returns>
        TBLLEntity? FirstOrDefault(TKey id, bool noTracking = true);

        /// <summary>
        ///     Asynchronously get entity by it's ID value
        /// </summary>
        /// <param name="id">Entity's ID value</param>
        /// <param name="noTracking">Disable tracking of returned entity</param>
        /// <returns>Task, which resolves to entity</returns>
        Task<TBLLEntity?> FirstOrDefaultAsync(TKey id, bool noTracking = true);

        /// <summary>
        ///     Begin tracking new entity
        /// </summary>
        /// <param name="entity">Entity to be tracked</param>
        /// <returns>Tracked entity</returns>
        TBLLEntity Add(TBLLEntity entity);

        /// <summary>
        ///     Asynchronously begin tracking new entity
        /// </summary>
        /// <param name="entity">Entity to be tracked</param>
        /// <returns>Task, which resolves to tracked entity</returns>
        Task<TBLLEntity> AddAsync(TBLLEntity entity);

        /// <summary>
        ///     Start tracking entity as updated
        /// </summary>
        /// <param name="entity">Entity to be updated</param>
        /// <returns>Updated entity</returns>
        TBLLEntity Update(TBLLEntity entity);

        /// <summary>
        ///     Remove entity
        /// </summary>
        /// <param name="id">Entity's ID value</param>
        /// <returns>Removed entity</returns>
        TBLLEntity? Remove(TKey id);

        /// <summary>
        ///     Remove entity
        /// </summary>
        /// <param name="entity">Entity to be removed</param>
        /// <returns>Removed entity</returns>
        TBLLEntity Remove(TBLLEntity entity);

        /// <summary>
        ///     Check entity's existence
        /// </summary>
        /// <param name="id">Entity's ID value</param>
        /// <returns>true/false</returns>
        bool Exists(TKey id);

        /// <summary>
        ///     Asynchronously check entity's existence
        /// </summary>
        /// <param name="id">Entity's ID value</param>
        /// <returns>true/false</returns>
        Task<bool> ExistsAsync(TKey id);
    }
}