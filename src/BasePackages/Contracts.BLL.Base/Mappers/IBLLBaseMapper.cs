﻿using System;
using kevain.BasePackages.Contracts.BLL.Base.Entities;
using kevain.BasePackages.Contracts.DAL.Base.Entities;
using kevain.BasePackages.Contracts.Mapper.Base;

namespace kevain.BasePackages.Contracts.BLL.Base.Mappers
{
    /// <summary>
    ///     BLL-level mapper for mapping entity back and forth between DAL and BLL DTOs
    /// </summary>
    /// <typeparam name="TDALEntity">Type of DAL entity</typeparam>
    /// <typeparam name="TBLLEntity">Type of BLL entity</typeparam>
    public interface IBLLBaseMapper<TDALEntity, TBLLEntity> : IBLLBaseMapper<Guid, TDALEntity, TBLLEntity>
        where TDALEntity : class, IDALEntity, new()
        where TBLLEntity : class, IBLLEntity, new()
    {
    }

    /// <summary>
    ///     BLL-level mapper for mapping entity back and forth between DAL and BLL DTOs
    /// </summary>
    /// <typeparam name="TKey">Entity's ID type</typeparam>
    /// <typeparam name="TDALEntity">Type of DAL entity</typeparam>
    /// <typeparam name="TBLLEntity">Type of BLL entity</typeparam>
    public interface IBLLBaseMapper<in TKey, TDALEntity, TBLLEntity> : IBaseMapper<TDALEntity, TBLLEntity>
        where TDALEntity : class, IDALEntity<TKey>, new()
        where TBLLEntity : class, IBLLEntity<TKey>, new()
        where TKey : IEquatable<TKey>
    {
    }
}