﻿using System;

namespace kevain.BasePackages.Contracts.BLL.Base.Entities
{
    /// <summary>
    ///     BLL entity with GUID-based Id property
    /// </summary>
    public interface IBLLEntity : IBLLEntity<Guid>
    {
    }

    /// <summary>
    ///     BLL entity with Id property
    /// </summary>
    /// <typeparam name="TKey">Type of Id property</typeparam>
    public interface IBLLEntity<TKey>
        where TKey : IEquatable<TKey>
    {
        /// <summary>
        ///     Entity's Identifier
        /// </summary>
        TKey Id { get; set; }
    }
}