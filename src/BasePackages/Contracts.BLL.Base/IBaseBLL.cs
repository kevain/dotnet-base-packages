﻿using System;
using System.Threading.Tasks;
using kevain.BasePackages.Contracts.BLL.Base.Services;

namespace kevain.BasePackages.Contracts.BLL.Base
{
    /// <summary>
    ///     Defines a basic BLL class
    /// </summary>
    public interface IBaseBLL
    {
        /// <summary>
        ///     Save changes.
        /// </summary>
        /// <returns>Number of affected items</returns>
        int SaveChanges();

        /// <summary>
        ///     Save changes asynchronously.
        /// </summary>
        /// <returns>Task, which resolves to number of affected items</returns>
        Task<int> SaveChangesAsync();

        /// <summary>
        ///     Get service of provided type. Initialize if it does not exist yet.
        /// </summary>
        /// <param name="serviceInitializationMethod">Method to create the service in case it does not exist yet</param>
        /// <typeparam name="TService">Type of service</typeparam>
        /// <returns>Service</returns>
        TService GetService<TService>(Func<TService> serviceInitializationMethod)
            where TService : class, IBaseService;
    }
}