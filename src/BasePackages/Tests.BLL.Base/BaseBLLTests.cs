using System.Threading.Tasks;
using kevain.BasePackages.BLL.Base;
using kevain.BasePackages.Contracts.BLL.Base;
using kevain.BasePackages.Contracts.BLL.Base.Services;
using kevain.BasePackages.Contracts.DAL.Base.UnitOfWork;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;

namespace kevain.BasePackages.Tests.BLL.Base
{
    [TestClass]
    public class BaseBLLTests
    {
        private IBaseBLL _bll = null!;
        private IBaseUnitOfWork _uow = null!;

        [TestMethod]
        public void GetService_InitializingNewService_ReturnsExpectedService()
        {
            _uow = new Mock<IBaseUnitOfWork>().Object;
            _bll = new Mock<BaseBLL<IBaseUnitOfWork>>(_uow).Object;

            var cachedService = _bll.GetService(() => new Mock<IBaseService>().Object);

            Assert.IsInstanceOfType(cachedService, typeof(IBaseService));
        }

        [TestMethod]
        public void GetService_QueryingServiceMultipleTimes_ReturnsSameInstances()
        {
            _uow = new Mock<IBaseUnitOfWork>().Object;
            _bll = new Mock<BaseBLL<IBaseUnitOfWork>>(_uow).Object;

            var cachedService1 = _bll.GetService(() => new Mock<IBaseService>().Object);
            var cachedService2 = _bll.GetService(() => new Mock<IBaseService>().Object);

            Assert.AreSame(cachedService1, cachedService2);
        }

        [TestMethod]
        public void SaveChanges_NoChanges_ReturnsZero()
        {
            const int expectedChanges = 0;

            var mockUoW = new Mock<IBaseUnitOfWork>();
            mockUoW
                .Setup(uow => uow.SaveChanges())
                .Returns(expectedChanges);

            _uow = mockUoW.Object;
            _bll = new Mock<BaseBLL<IBaseUnitOfWork>>(_uow).Object;

            var actualChanges = _bll.SaveChanges();

            Assert.AreEqual(expectedChanges, actualChanges);
        }

        [TestMethod]
        public void SaveChanges_OneChange_ReturnsOne()
        {
            const int expectedChanges = 1;

            var mockUoW = new Mock<IBaseUnitOfWork>();
            mockUoW
                .Setup(uow => uow.SaveChanges())
                .Returns(expectedChanges);

            _uow = mockUoW.Object;
            _bll = new Mock<BaseBLL<IBaseUnitOfWork>>(_uow).Object;

            var actualChanges = _bll.SaveChanges();

            Assert.AreEqual(expectedChanges, actualChanges);
        }

        [TestMethod]
        public async Task SaveChangesAsync_NoChanges_ReturnsZero()
        {
            const int expectedChanges = 0;

            var mockUoW = new Mock<IBaseUnitOfWork>();
            mockUoW
                .Setup(uow => uow.SaveChangesAsync())
                .ReturnsAsync(expectedChanges);

            _uow = mockUoW.Object;
            _bll = new Mock<BaseBLL<IBaseUnitOfWork>>(_uow).Object;

            var actualChanges = await _bll.SaveChangesAsync();

            Assert.AreEqual(expectedChanges, actualChanges);
        }

        [TestMethod]
        public async Task SaveChangesAsync_OneChange_ReturnsOne()
        {
            const int expectedChanges = 1;

            var mockUoW = new Mock<IBaseUnitOfWork>();
            mockUoW
                .Setup(uow => uow.SaveChangesAsync())
                .ReturnsAsync(expectedChanges);

            _uow = mockUoW.Object;
            _bll = new Mock<BaseBLL<IBaseUnitOfWork>>(_uow).Object;

            var actualChanges = await _bll.SaveChangesAsync();

            Assert.AreEqual(expectedChanges, actualChanges);
        }
    }
}