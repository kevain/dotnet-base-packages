﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using kevain.BasePackages.BLL.Base.Services;
using kevain.BasePackages.Contracts.BLL.Base.Mappers;
using kevain.BasePackages.Contracts.DAL.Base.Repositories;
using kevain.BasePackages.Contracts.DAL.Base.UnitOfWork;
using kevain.BasePackages.Tests.BLL.Base.Helpers.Entities;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;

namespace kevain.BasePackages.Tests.BLL.Base
{
    [TestClass]
    public class TestBLLEntityServiceTests
    {
        private Mock<BaseEntityService<Guid, IBaseUnitOfWork, IBaseRepository<Guid, TestDALEntity>,
            IBLLBaseMapper<Guid, TestDALEntity, TestBLLEntity>, TestDALEntity, TestBLLEntity>> _mockEntityService =
            null!;

        private Mock<IBLLBaseMapper<Guid, TestDALEntity, TestBLLEntity>> _mockMapper = null!;
        private Mock<IBaseRepository<Guid, TestDALEntity>> _mockRepo = null!;
        private Mock<IBaseUnitOfWork> _mockUoW = null!;

        [TestInitialize]
        public void BeforeTest()
        {
            _mockUoW = new Mock<IBaseUnitOfWork>();
            _mockRepo = new Mock<IBaseRepository<Guid, TestDALEntity>>();
            _mockMapper = new Mock<IBLLBaseMapper<Guid, TestDALEntity, TestBLLEntity>>();
            _mockEntityService =
                new Mock<BaseEntityService<Guid, IBaseUnitOfWork, IBaseRepository<Guid, TestDALEntity>,
                        IBLLBaseMapper<Guid, TestDALEntity, TestBLLEntity>, TestDALEntity, TestBLLEntity>>
                    (_mockUoW.Object, _mockRepo.Object, _mockMapper.Object) {CallBase = true};
        }

        [TestMethod]
        public void Exists_EntityWithIdExists_ReturnsTrue()
        {
            // Arrange
            _mockRepo
                .Setup(r => r.Exists(It.IsAny<Guid>()))
                .Returns(true);

            // Act
            var entityExists = _mockEntityService.Object.Exists(Guid.NewGuid());

            // Assert
            Assert.IsTrue(entityExists);
        }

        [TestMethod]
        public void Exists_EntityWithIdDoesNotExist_ReturnsFalse()
        {
            _mockRepo
                .Setup(r => r.Exists(It.IsAny<Guid>()))
                .Returns(false);

            var entityExists = _mockEntityService.Object.Exists(Guid.NewGuid());

            Assert.IsFalse(entityExists);
        }

        [TestMethod]
        public async Task ExistsAsync_EntityWithIdExists_ReturnsTrue()
        {
            _mockRepo
                .Setup(r => r.ExistsAsync(It.IsAny<Guid>()))
                .ReturnsAsync(true);

            var entityExists = await _mockEntityService.Object.ExistsAsync(Guid.NewGuid());

            Assert.IsTrue(entityExists);
        }

        [TestMethod]
        public async Task ExistsAsync_EntityWithIdDoesNotExist_ReturnsFalse()
        {
            _mockRepo
                .Setup(r => r.ExistsAsync(It.IsAny<Guid>()))
                .ReturnsAsync(false);

            var entityExists = await _mockEntityService.Object.ExistsAsync(Guid.NewGuid());

            Assert.IsFalse(entityExists);
        }

        [TestMethod]
        public void GetAll_NoEntities_ReturnsEmptyList()
        {
            // Arrange
            const int expectedCount = 0;
            _mockRepo
                .Setup(r => r.GetAll(true))
                .Returns(() => new List<TestDALEntity>());

            _mockMapper
                .Setup(m => m.Map(It.IsAny<TestDALEntity>()))
                .Returns(() => new TestBLLEntity());

            // Act
            var bllEntities = _mockEntityService.Object.GetAll();
            var actualCount = bllEntities.Count();

            // Assert
            Assert.AreEqual(expectedCount, actualCount);
        }

        [TestMethod]
        public void GetAll_HasEntities_ReturnsAllEntities()
        {
            // Arrange
            var dalEntities = new List<TestDALEntity> {new TestDALEntity(), new TestDALEntity()};
            var expectedCount = dalEntities.Count();

            _mockRepo
                .Setup(r => r.GetAll(true))
                .Returns(dalEntities);

            _mockMapper
                .Setup(m => m.Map(It.IsAny<TestDALEntity>()))
                .Returns(() => new TestBLLEntity());

            // Act
            var bllEntities = _mockEntityService.Object.GetAll();
            var actualCount = bllEntities.Count();

            // Assert
            Assert.AreEqual(expectedCount, actualCount);
        }

        [TestMethod]
        public async Task GetAllAsync_NoEntities_ReturnsEmptyList()
        {
            // Arrange
            const int expectedCount = 0;
            _mockRepo
                .Setup(r => r.GetAllAsync(true))
                .ReturnsAsync(() => new List<TestDALEntity>());

            _mockMapper
                .Setup(m => m.Map(It.IsAny<TestDALEntity>()))
                .Returns(() => new TestBLLEntity());

            // Act
            var bllEntities = await _mockEntityService.Object.GetAllAsync();
            var actualCount = bllEntities.Count();

            // Assert
            Assert.AreEqual(expectedCount, actualCount);
        }

        [TestMethod]
        public async Task GetAllAsync_HasEntities_ReturnsAllEntities()
        {
            // Arrange
            var dalEntities = new List<TestDALEntity> {new TestDALEntity(), new TestDALEntity()};
            var expectedCount = dalEntities.Count();

            _mockRepo
                .Setup(r => r.GetAllAsync(true))
                .ReturnsAsync(dalEntities);

            _mockMapper
                .Setup(m => m.Map(It.IsAny<TestDALEntity>()))
                .Returns(() => new TestBLLEntity());

            // Act
            var bllEntities = await _mockEntityService.Object.GetAllAsync();
            var actualCount = bllEntities.Count();

            // Assert
            Assert.AreEqual(expectedCount, actualCount);
        }

        [TestMethod]
        public void FirstOrDefault_EntityDoesNotExist_ReturnsNull()
        {
            // Arrange
            _mockRepo
                .Setup(r => r.FirstOrDefault(It.IsAny<Guid>(), It.IsAny<bool>()))
                .Returns(() => null);

            // Act
            var entity = _mockEntityService.Object.FirstOrDefault(Guid.NewGuid());

            // Assert
            Assert.IsNull(entity);
        }

        [TestMethod]
        public void FirstOrDefault_EntityExists_ReturnsEntity()
        {
            // Arrange
            _mockRepo
                .Setup(r => r.FirstOrDefault(It.IsAny<Guid>(), It.IsAny<bool>()))
                .Returns(() => new TestDALEntity());

            _mockMapper
                .Setup(m => m.Map(It.IsAny<TestDALEntity>()))
                .Returns(() => new TestBLLEntity());

            // Act
            var entity = _mockEntityService.Object.FirstOrDefault(Guid.NewGuid());

            // Assert
            Assert.IsNotNull(entity);
        }

        [TestMethod]
        public async Task FirstOrDefaultAsync_EntityDoesNotExist_ReturnsNull()
        {
            // Arrange
            _mockRepo
                .Setup(r => r.FirstOrDefaultAsync(It.IsAny<Guid>(), It.IsAny<bool>()))
                .ReturnsAsync(() => null);

            // Act
            var entity = await _mockEntityService.Object.FirstOrDefaultAsync(Guid.NewGuid());

            // Assert
            Assert.IsNull(entity);
        }

        [TestMethod]
        public async Task FirstOrDefaultAsync_EntityExists_ReturnsEntity()
        {
            // Arrange
            _mockRepo
                .Setup(r => r.FirstOrDefaultAsync(It.IsAny<Guid>(), It.IsAny<bool>()))
                .ReturnsAsync(() => new TestDALEntity());

            _mockMapper
                .Setup(m => m.Map(It.IsAny<TestDALEntity>()))
                .Returns(() => new TestBLLEntity());

            // Act
            var entity = await _mockEntityService.Object.FirstOrDefaultAsync(Guid.NewGuid());

            // Assert
            Assert.IsNotNull(entity);
        }


        [TestMethod]
        public void Add_ReturnsAddedEntity()
        {
            // Arrange
            _mockRepo
                .Setup(r => r.Add(It.IsAny<TestDALEntity>()))
                .Returns((TestDALEntity e) => e);

            _mockMapper
                .Setup(m => m.Map(It.IsAny<TestDALEntity>()))
                .Returns(() => new TestBLLEntity());

            _mockMapper
                .Setup(m => m.Map(It.IsAny<TestBLLEntity>()))
                .Returns(() => new TestDALEntity());

            // Act
            var addedEntity = _mockEntityService.Object.Add(new TestBLLEntity());

            // Assert
            Assert.IsNotNull(addedEntity);
        }

        [TestMethod]
        public async Task AddAsync_ReturnsAddedEntity()
        {
            // Arrange
            _mockRepo
                .Setup(r => r.AddAsync(It.IsAny<TestDALEntity>()))
                .ReturnsAsync((TestDALEntity e) => e);

            _mockMapper
                .Setup(m => m.Map(It.IsAny<TestDALEntity>()))
                .Returns(() => new TestBLLEntity());

            _mockMapper
                .Setup(m => m.Map(It.IsAny<TestBLLEntity>()))
                .Returns(() => new TestDALEntity());

            // Act
            var addedEntity = await _mockEntityService.Object.AddAsync(new TestBLLEntity());

            // Assert
            Assert.IsNotNull(addedEntity);
        }

        [TestMethod]
        public void Update_ReturnsAddedEntity()
        {
            // Arrange
            _mockRepo
                .Setup(r => r.Update(It.IsAny<TestDALEntity>()))
                .Returns((TestDALEntity e) => e);

            _mockMapper
                .Setup(m => m.Map(It.IsAny<TestDALEntity>()))
                .Returns(() => new TestBLLEntity());

            _mockMapper
                .Setup(m => m.Map(It.IsAny<TestBLLEntity>()))
                .Returns(() => new TestDALEntity());

            // Act
            var updatedEntity = _mockEntityService.Object.Update(new TestBLLEntity());

            // Assert
            Assert.IsNotNull(updatedEntity);
        }

        [TestMethod]
        public void Remove_EntityByObject_ReturnsAddedEntity()
        {
            // Arrange
            _mockRepo
                .Setup(r => r.Remove(It.IsAny<TestDALEntity>()))
                .Returns((TestDALEntity e) => e);

            _mockMapper
                .Setup(m => m.Map(It.IsAny<TestDALEntity>()))
                .Returns(() => new TestBLLEntity());

            _mockMapper
                .Setup(m => m.Map(It.IsAny<TestBLLEntity>()))
                .Returns(() => new TestDALEntity());

            // Act
            var removedEntity = _mockEntityService.Object.Remove(new TestBLLEntity());

            // Assert
            Assert.IsNotNull(removedEntity);
        }

        [TestMethod]
        public void Remove_EntityById_ReturnsAddedEntity()
        {
            // Arrange
            _mockRepo
                .Setup(r => r.Remove(It.IsAny<Guid>()))
                .Returns(() => new TestDALEntity());

            _mockMapper
                .Setup(m => m.Map(It.IsAny<TestDALEntity>()))
                .Returns(() => new TestBLLEntity());

            // Act
            var removedEntity = _mockEntityService.Object.Remove(Guid.NewGuid());

            // Assert
            Assert.IsNotNull(removedEntity);
        }

        [TestMethod]
        public void Remove_EntityByIdDoesNotExist_ReturnsAddedEntity()
        {
            // Arrange
            _mockRepo
                .Setup(r => r.Remove(It.IsAny<Guid>()))
                .Returns(() => null);

            // Act
            var removedEntity = _mockEntityService.Object.Remove(Guid.NewGuid());

            // Assert
            Assert.IsNull(removedEntity);
        }
    }
}