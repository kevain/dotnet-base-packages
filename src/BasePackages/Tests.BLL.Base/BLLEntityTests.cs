﻿using System;
using kevain.BasePackages.BLL.Base.Entities;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;

namespace kevain.BasePackages.Tests.BLL.Base
{
    [TestClass]
    public class BLLEntityTests
    {
        [TestMethod]
        public void Init_NoTypeParameter_IdPropertyTypeShouldBeGuid()
        {
            var entity = new Mock<BLLEntity>().Object;

            Assert.IsInstanceOfType(entity.Id, typeof(Guid));
        }

        [TestMethod]
        public void Init_TypeParameterIsGuid_IdPropertyTypeShouldBeGuid()
        {
            var entity = new Mock<BLLEntity<Guid>>().Object;

            Assert.IsInstanceOfType(entity.Id, typeof(Guid));
        }

        [TestMethod]
        public void Init_NoTypeParameterIsInt_IdPropertyTypeShouldBeGuid()
        {
            var entity = new Mock<BLLEntity<int>>().Object;

            Assert.IsInstanceOfType(entity.Id, typeof(int));
        }
    }
}