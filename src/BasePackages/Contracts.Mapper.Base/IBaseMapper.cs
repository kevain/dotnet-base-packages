﻿namespace kevain.BasePackages.Contracts.Mapper.Base
{
    /// <summary>
    ///     Mapper for mapping objects back and forth between two types
    /// </summary>
    /// <typeparam name="TFirstObject">Type of first object</typeparam>
    /// <typeparam name="TSecondObject">Type of second object</typeparam>
    public interface IBaseMapper<TFirstObject, TSecondObject>
        where TFirstObject : class?, new()
        where TSecondObject : class?, new()
    {
        /// <summary>
        ///     Map object from provided second type to provided first type
        /// </summary>
        /// <param name="source">Object to be mapped</param>
        /// <returns>Mapped object of first provided type</returns>
        TFirstObject Map(TSecondObject source);

        /// <summary>
        ///     Map object from provided first type to provided second type.
        /// </summary>
        /// <param name="source">Object to be mapped</param>
        /// <returns>Mapped object of second provided type</returns>
        TSecondObject Map(TFirstObject source);
    }

    /// <summary>
    ///     Mapper for mapping objects back and forth between three types
    /// </summary>
    /// <typeparam name="TFirstObject">Type of first object</typeparam>
    /// <typeparam name="TSecondObject">Type of second object</typeparam>
    /// <typeparam name="TThirdObject">Type of third object</typeparam>
    public interface IBaseMapper<in TFirstObject, in TSecondObject, in TThirdObject>
        where TFirstObject : class?, new()
        where TSecondObject : class?, new()
        where TThirdObject : class?, new()
    {
        /// <summary>
        ///     Map object from first type to second or third.
        /// </summary>
        /// <param name="source">Object to be mapped</param>
        /// <typeparam name="TDestination">Mapping destination type</typeparam>
        /// <returns>Mapped object of provided destination type</returns>
        TDestination Map<TDestination>(TFirstObject source) where TDestination : class?, new();

        /// <summary>
        ///     Map object from second type to first or third
        /// </summary>
        /// <param name="source">Object to be mapped</param>
        /// <typeparam name="TDestination">Mapping destination type</typeparam>
        /// <returns>Mapped object of provided destination type</returns>
        TDestination Map<TDestination>(TSecondObject source) where TDestination : class?, new();

        /// <summary>
        ///     Map object from third type to first or second
        /// </summary>
        /// <param name="source">Object to be mapped</param>
        /// <typeparam name="TDestination">Mapping destination type</typeparam>
        /// <returns>Mapped object of provided destination type</returns>
        TDestination Map<TDestination>(TThirdObject source) where TDestination : class?, new();
    }
}