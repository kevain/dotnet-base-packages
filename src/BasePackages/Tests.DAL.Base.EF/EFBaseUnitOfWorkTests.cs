using System.Threading.Tasks;
using kevain.BasePackages.Tests.DAL.Base.EF.Helpers.DbContext;
using kevain.BasePackages.Tests.DAL.Base.EF.Helpers.Entities;
using kevain.BasePackages.Tests.DAL.Base.EF.Helpers.UoW;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace kevain.BasePackages.Tests.DAL.Base.EF
{
    [TestClass]
    public class EFBaseUnitOfWorkTests
    {
        private readonly TestEFDbContext _dbContext = new TestEFDbContext();

        [TestInitialize]
        public void BeforeTest() => _dbContext.Database.EnsureCreated();

        [TestCleanup]
        public void AfterTest() => _dbContext.Database.EnsureDeleted();
        
        [TestMethod]
        public void SaveChanges_NoChanges_ReturnsZero()
        {
            var uow = new TestEFUnitOfWork(_dbContext);
            const int expected = 0;

            var actual = uow.SaveChanges();

            Assert.AreEqual(expected, actual);
        }

        [TestMethod]
        public async Task SaveChangesAsync_NoChanges_ReturnsZero()
        {
            var uow = new TestEFUnitOfWork(new TestEFDbContext());
            const int expected = 0;

            var actual = await uow.SaveChangesAsync();

            Assert.AreEqual(expected, actual);
        }

        [TestMethod]
        public void SaveChanges_OneAddition_ReturnsOne()
        {
            _dbContext.Add(new TestDomainEntity());
            
            var uow = new TestEFUnitOfWork(_dbContext);
            const int expected = 1;

            var actual = uow.SaveChanges();

            Assert.AreEqual(expected, actual);
        }
        
        [TestMethod]
        public async Task SaveChangesAsync_OneAddition_ReturnsOne()
        {
            _dbContext.Add(new TestDomainEntity());
            
            var uow = new TestEFUnitOfWork(_dbContext);
            const int expected = 1;

            var actual = await uow.SaveChangesAsync();

            Assert.AreEqual(expected, actual);
        }
    }
}