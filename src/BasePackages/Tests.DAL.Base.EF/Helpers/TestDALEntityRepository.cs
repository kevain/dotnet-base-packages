﻿using System;
using kevain.BasePackages.DAL.Base.EF.Repository;
using kevain.BasePackages.DAL.Base.Mappers;
using kevain.BasePackages.Tests.DAL.Base.EF.Helpers.DbContext;
using kevain.BasePackages.Tests.DAL.Base.EF.Helpers.Entities;

namespace kevain.BasePackages.Tests.DAL.Base.EF.Helpers
{
    public class TestDALEntityRepository : EFBaseRepository<Guid, TestEFDbContext, TestDomainEntity, TestDALEntity>
    {
        public TestDALEntityRepository(TestEFDbContext repositoryDbContext) : base(repositoryDbContext,
            new DALBaseMapper<TestDomainEntity, TestDALEntity>())
        {
        }
    }
}