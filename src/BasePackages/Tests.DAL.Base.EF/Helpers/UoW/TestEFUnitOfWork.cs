﻿using kevain.BasePackages.DAL.Base.EF.UnitOfWork;
using kevain.BasePackages.Tests.DAL.Base.EF.Helpers.DbContext;

namespace kevain.BasePackages.Tests.DAL.Base.EF.Helpers.UoW
{
    public class TestEFUnitOfWork : EFBaseUnitOfWork<TestEFDbContext>
    {
        public TestEFUnitOfWork(TestEFDbContext uoWDbContext) : base(uoWDbContext)
        {
        }
        
        
    }
}