﻿using kevain.BasePackages.Domain.Base;

namespace kevain.BasePackages.Tests.DAL.Base.EF.Helpers.Entities
{
    public class TestDomainEntity : DomainEntity
    {
        public string? Text { get; set; }
    }
}