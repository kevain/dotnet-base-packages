﻿using kevain.BasePackages.DAL.Base.Entities;

namespace kevain.BasePackages.Tests.DAL.Base.EF.Helpers.Entities
{
    public class TestDALEntity : DALEntity
    {
        public string? Text { get; set; }
    }
}