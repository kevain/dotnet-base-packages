﻿using kevain.BasePackages.Tests.DAL.Base.EF.Helpers.Entities;
using Microsoft.EntityFrameworkCore;

namespace kevain.BasePackages.Tests.DAL.Base.EF.Helpers.DbContext
{
    public class TestEFDbContext : Microsoft.EntityFrameworkCore.DbContext
    {
        public DbSet<TestDomainEntity> DomainEntities { get; set; } = default!;

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseSqlite("Data Source=TestsDALBaseEF.db");
        }
    }
}