﻿using System;
using System.Linq;
using System.Threading.Tasks;
using kevain.BasePackages.Tests.DAL.Base.EF.Helpers;
using kevain.BasePackages.Tests.DAL.Base.EF.Helpers.DbContext;
using kevain.BasePackages.Tests.DAL.Base.EF.Helpers.Entities;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace kevain.BasePackages.Tests.DAL.Base.EF
{
    [TestClass]
    public class EFBaseRepositoryTests
    {
        private TestEFDbContext _dbContext = null!;
        private TestDALEntityRepository _repository = null!;

        [TestInitialize]
        public void BeforeTest()
        {
            _dbContext = new TestEFDbContext();
            _dbContext.Database.EnsureCreated();
            _repository = new TestDALEntityRepository(_dbContext);
        }

        [TestCleanup]
        public void AfterTest()
        {
            _dbContext.Database.EnsureDeleted();
            _dbContext.Dispose();
            _repository = null!;
        }

        private void ReloadDatabaseContext()
        {
            _dbContext.Dispose();
            _dbContext = new TestEFDbContext();
            _repository = new TestDALEntityRepository(_dbContext);
        }

        [TestMethod]
        public void GetAll_NoEntities_ReturnsEmptyList()
        {
            const int expectedCount = 0;

            var items = _repository.GetAll();
            var actualCount = items.Count();

            Assert.AreEqual(expectedCount, actualCount);
        }

        [TestMethod]
        public void GetAll_ReturnsAllEntities()
        {
            var expectedCount = Faker.RandomNumber.Next(5, 10);

            // Seed items
            for (var i = 0; i < expectedCount; i++)
            {
                _dbContext.Add(new TestDomainEntity());
            }

            _dbContext.SaveChanges();

            var items = _repository.GetAll();
            var actualCount = items.Count();

            Assert.AreEqual(expectedCount, actualCount, $"Expected: {expectedCount}, actual: {actualCount}");
        }

        [TestMethod]
        public async Task GetAllAsync_NoEntities_ReturnsEmptyList()
        {
            const int expectedCount = 0;

            var items = await _repository.GetAllAsync();
            var actualCount = items.Count();

            Assert.AreEqual(expectedCount, actualCount);
        }

        [TestMethod]
        public async Task GetAllAsync_ReturnsAllEntities()
        {
            var expectedCount = Faker.RandomNumber.Next(5, 10);

            // Seed items
            for (var i = 0; i < expectedCount; i++)
            {
                _dbContext.Add(new TestDomainEntity());
            }

            await _dbContext.SaveChangesAsync();

            var items = await _repository.GetAllAsync();
            var actualCount = items.Count();

            Assert.AreEqual(expectedCount, actualCount, $"Expected: {expectedCount}, actual: {actualCount}");
        }

        [TestMethod]
        public void FirstOrDefault_EntityDoesNotExist_ReturnsNull()
        {
            var shouldBeNull = _repository.FirstOrDefault(Guid.NewGuid());

            Assert.IsNull(shouldBeNull);
        }

        [TestMethod]
        public async Task FirstOrDefaultAsync_EntityDoesNotExist_ReturnsNull()
        {
            var shouldBeNull = await _repository.FirstOrDefaultAsync(Guid.NewGuid());

            Assert.IsNull(shouldBeNull);
        }

        [TestMethod]
        public void Add_InitNewEntity_EntityIsTrackedAndGetsIdAfterSave()
        {
            var entity = _repository.Add(new TestDALEntity());
            _dbContext.SaveChanges();

            Assert.AreNotEqual(Guid.Empty, entity.Id);
        }
        
        [TestMethod]
        public async Task AddAsync_InitNewEntity_EntityIsTrackedAndGetsIdAfterSave()
        {
            var entity = await _repository.AddAsync(new TestDALEntity());
            await _dbContext.SaveChangesAsync();

            Assert.AreNotEqual(Guid.Empty, entity.Id);
        }

        [TestMethod]
        public void Update_ReturnsUpdatedEntity()
        {
            // Seed entity
            var entity = new TestDomainEntity();
            _dbContext.DomainEntities.Add(entity);
            _dbContext.SaveChanges();
            
            // Reload Database context to clear entity tracker
            ReloadDatabaseContext();

            var dalEntity = _repository.FirstOrDefault(entity.Id);
            dalEntity!.Text = Faker.Lorem.Sentence();
            var updatedEntity = _repository.Update(dalEntity);

            Assert.AreEqual(dalEntity.Text, updatedEntity.Text);
        }
        
        [TestMethod]
        public void Remove_ByEntity_ReturnsRemovedEntity()
        {
            // Seed entity
            var entity = new TestDomainEntity();
            _dbContext.DomainEntities.Add(entity);
            _dbContext.SaveChanges();
            
            // Reload Database context to clear entity tracker
            ReloadDatabaseContext();

            var dalEntity = _repository.FirstOrDefault(entity.Id);
            var updatedEntity = _repository.Remove(dalEntity!);

            Assert.AreEqual(dalEntity!.Id, updatedEntity.Id);
        }
        
        [TestMethod]
        public void Remove_ById_ReturnsRemovedEntity()
        {
            // Seed entity
            var entity = new TestDomainEntity();
            _dbContext.DomainEntities.Add(entity);
            _dbContext.SaveChanges();
            
            // Reload Database context to clear entity tracker
            ReloadDatabaseContext();

            var updatedEntity = _repository.Remove(entity.Id);

            Assert.AreEqual(entity.Id, updatedEntity!.Id);
        }
        
        [TestMethod]
        public void Remove_ByIdNotExists_ReturnsNull()
        {
            var updatedEntity = _repository.Remove(Guid.NewGuid());

            Assert.IsNull(updatedEntity);
        }

        [TestMethod]
        public void Exists_DoesExist_ReturnsTrue()
        {
            // Seed entity
            var entity = new TestDomainEntity();
            _dbContext.DomainEntities.Add(entity);
            _dbContext.SaveChanges();
            
            ReloadDatabaseContext();

            var entityExists = _repository.Exists(entity.Id);
            
            Assert.IsTrue(entityExists);
        }
        
        [TestMethod]
        public void Exists_DoesNotExist_ReturnsFalse()
        {
            var entityExists = _repository.Exists(Guid.NewGuid());
            
            Assert.IsFalse(entityExists);
        }
        
        [TestMethod]
        public async Task ExistsAsync_DoesExist_ReturnsTrue()
        {
            // Seed entity
            var entity = new TestDomainEntity();
            await _dbContext.DomainEntities.AddAsync(entity);
            await _dbContext.SaveChangesAsync();
            
            ReloadDatabaseContext();

            var entityExists = await _repository.ExistsAsync(entity.Id);
            
            Assert.IsTrue(entityExists);
        }
        
        [TestMethod]
        public async Task ExistsAsync_DoesNotExist_ReturnsFalse()
        {
            var entityExists = await _repository.ExistsAsync(Guid.NewGuid());
            
            Assert.IsFalse(entityExists);
        }
    }
}