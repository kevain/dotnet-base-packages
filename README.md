# BasePackages
Interfaces and their implementations, which provide base classes to Domain, Data Access and Business Logic layers. Said classes contain commonly used properties/methods on their respective layers, thus reducing development time, when starting on a new project. Extra functionality can be easily added by deriving from base classes. 

Solution is based off of the course project & lecture materials for the 'Building Distributed Systems' course @ TalTech IT College, as such - credit to the [lecturer](https://git.akaver.com/akaver), whose code I have used and modified to better suit my needs. 

## Contents

| Package | Description | Nuget |
| ----------- | --------------- | --------- |
| [Contracts.Domain.Base](./src/BasePackages/Contracts.Domain.Base/) | Interfaces for domain level entity | [nuget](https://www.nuget.org/packages/kevain.BasePackages.Contracts.Domain.Base/) |
| [Domain.Base](./src/BasePackages/Contracts.Domain.Base/) | Implementation(s) of domain level entity interfaces | [nuget](https://www.nuget.org/packages/kevain.BasePackages.Domain.Base/) |
| [Contracts.DAL.Base](./src/BasePackages/Contracts.DAL.Base/) | Interfaces for a base repository and base unit of work class | [nuget](https://www.nuget.org/packages/kevain.BasePackages.Contracts.DAL.Base/) |
| [DAL.Base](./src/BasePackages/DAL.Base/) | Implementations of DAL layer mapper and unit of work class | [nuget](https://www.nuget.org/packages/kevain.BasePackages.DAL.Base/) |
| [DAL.Base.EF](./src/BasePackages/DAL.Base.EF/) | Base implementation of an EF Core based repository and Unit of Work class | [nuget](https://www.nuget.org/packages/kevain.BasePackages.DAL.Base.EF/) |
| [Contracts.BLL.Base](./src/BasePackages/Contracts.BLL.Base/) | Interfaces for BLL layer mapper, services and base BLL class | [nuget](https://www.nuget.org/packages/kevain.BasePackages.Contracts.BLL.Base/) |
| [BLL.Base](./src/BasePackages/BLL.Base/) | Base implementation of corresponding interfaces | [nuget](https://www.nuget.org/packages/kevain.BasePackages.BLL.Base/) |
| [Contracts.Mapper.Base](./src/BasePackages/Contracts.Mapper.Base/) | Interface for basic mapper for mapping back and forth between two types | [nuget](https://www.nuget.org/packages/kevain.BasePackages.Contracts.Mapper.Base/) |
| [Mapper.Base](./src/BasePackages/Mapper.Base/) | Implementation of basic mapper, which can map automatically between two types. Based on AutoMapper | [nuget](https://www.nuget.org/packages/kevain.BasePackages.Mapper.Base/) |
